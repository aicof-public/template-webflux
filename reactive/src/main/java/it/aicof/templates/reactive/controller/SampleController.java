package it.aicof.templates.reactive.controller;

import it.aicof.templates.reactive.model.Sample;
import it.aicof.templates.reactive.service.SampleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/sample")
@Slf4j
@RequiredArgsConstructor
public class SampleController {

    private final SampleService sampleService;

    @GetMapping(value = "/{id}")
    public Mono<Sample> get(@PathVariable String id) {
        return sampleService.get(id);
    }

    @GetMapping
    public Flux<Sample> getAll() {
        return sampleService.getAll();
    }
}
