package it.aicof.templates.reactive.service;

import it.aicof.templates.reactive.model.Sample;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SampleService {

    Mono<Sample> get(String id);

    Flux<Sample> getAll();
}
