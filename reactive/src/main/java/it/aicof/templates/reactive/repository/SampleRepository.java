package it.aicof.templates.reactive.repository;

import it.aicof.templates.reactive.model.Sample;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface SampleRepository extends ReactiveMongoRepository<Sample, String> {
  Flux<Sample> findAllByField(String field);
}
