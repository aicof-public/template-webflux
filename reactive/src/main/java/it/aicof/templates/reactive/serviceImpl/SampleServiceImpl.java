package it.aicof.templates.reactive.serviceImpl;

import it.aicof.templates.reactive.model.Sample;
import it.aicof.templates.reactive.repository.SampleRepository;
import it.aicof.templates.reactive.service.SampleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Service
public class SampleServiceImpl implements SampleService {

  private final SampleRepository repository;

  @Override
  public Mono<Sample> get(String id) {
    return this.repository.findById(id);
  }

  @Override
  public Flux<Sample> getAll() {
    return this.repository.findAll();
  }
}
